const http = require('http');
const fs = require('fs');
const { buffer } = require('stream/consumers');

//HTTP => request and response

//Hola mundo basico con HTTP y Servidor simple de hola mundo en node.js usando HTTP:
/*
http.createServer((request, response) => {
    response.writeHead(200, {'Content-Type': 'text/plain'});
    response.end('Hola Mundo\n');
}).listen(4444);
*/

/*  Servidor síncrono que lea y envíe un archivo
http.createServer((request, response) => {
    const file = request.url == '/' ? './WWW/index.html' : `./WWW/${request.url}`;

    try {
        const data = fs.readFileSync(file);
        response.writeHead(200, {'Content-Type': 'text/html'});
        response.write(data);
        response.end();
    } catch (err) {
        response.writeHead(404, {'Content-Type': 'text/html'});
        response.write('Not Found');
        response.end();
    }
}).listen(4444);

*/

/* Servidor asíncrono que lea y envíe un archivo:

http.createServer((request, response) => {
    const file = request.url == '/' ? './WWW/index.html' : `./WWW/${request.url}`;

    fs.readFile(file, (err, data) => {
        if (err) {
            response.writeHead(404, {'Content-Type': 'text/html'});
            response.write('Not Found');
            response.end();
        } else {
            response.writeHead(200, {'Content-Type': 'text/html'});
            response.write(data);
            response.end();
        }
    });
}).listen(4444);

*/

/*Servidor asíncrono que lea y envíe un archivo según su extensión:*/
 
http.createServer((request, response) => {
    const file = request.url == '/' ? './WWW/index.html' : `./WWW/${request.url}`
    
    if (request.url == '/registro') {
        let data = [];
        request.on("data", value => {
            data.push(value);
        }).on("end", ()=>{
            let params = Buffer.concat(data).toString()
            response.write(params);
            response.end();
        });
    }else{
        fs.readFile(file, (err,data)=>{
            if(err){
                response.writeHead(404, {"Content-Type":"text/html"});
                response.write("Not Found");
                response.end();
            }else{
                const extension = request.url.split('.').pop();
                switch (extension) {
                    case 'txt':
                            response.writeHead(200, {"Content-Type":"text/plain"});
                        break;
                    case 'html':
                            response.writeHead(200, {"Content-Type":"text/html"});
                        break;
                    case 'css':
                            response.writeHead(200, {"Content-Type":"text/css"});
                        break;
                    case 'js':
                            response.writeHead(200, {"Content-Type":"text/js"});
                        break;
                    case 'jpeg':
                            response.writeHead(200, {"Content-Type":"image/jpge"});
                        break;
                    case 'png':
                            response.writeHead(200, {"Content-Type":"image/png"});
                        break;
                    case 'jpg':
                            response.writeHead(200, {"Content-Type":"image/jpg"});
                        break;
                    default:
                            response.writeHead(200, {"Content-Type":"text/html"});
                        break;
                }
                response.write(data);
                response.end();
            }
            
        });
    }
}).listen(4444);
