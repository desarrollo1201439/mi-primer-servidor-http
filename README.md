# Servidor HTTP en Node.js

Este proyecto presenta varios ejemplos de servidores HTTP básicos implementados en Node.js utilizando el módulo http y operaciones del sistema de archivos (fs).
El servidor tiene el puerto de salida por el 4444.

## Autor

**Irvin Alejandro Morales Ramirez**  
**Materia: Desarrollo basado en plataformas**  
**grupo 6CC2**

## Funcionalidades

- El servidor síncrono leé y envía un archivo en la ruta predeterminada: /WWW/index.html para otras rutas: /WWW/( nombre-del-archivo )
- El servidor asíncrono leé y envía un archivo en la ruta predeterminada: /WWW/index.html para otras rutas: /WWW/( nombre-del-archivo )
- Ademas el servidor asíncrono leé y envía un archivo según su tipo de extensión
